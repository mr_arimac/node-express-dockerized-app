# Node Express Dockerized App

This is a sample node & expressjs web application that is going to be dockerized and store on Gitlab Container Registry.
## Prerequisites

- Make sure you have these installed

-  [node.js](http://nodejs.org/)

-  [git](http://git-scm.com/)


## Run Project Locally

CD to the application root directory `cd node-express-dockerized-app`  <br>

Run `> npm install -g nodemon` to install nodemod globally <br>

Run `> npm install` to install the project dependencies <br>

Run `> npm run dev` command to start the application in development environment <br>

Run `> npm run prod` command to start the application in production environment
## Run Project with Docker

### Create Docker Files

Create a file as `Dockerfile`  <br>

Add following content to it: <br>
```
FROM  node:latest
WORKDIR  /app/
COPY  package*.json  /app/
RUN  npm  install  -g  nodemon
RUN  npm  install
COPY  .  .
CMD  npm  start
```
Then create a `.dockerignore` file to avoid unnecessary files as follows:
```
node_modules
npm-debug.log
```

### Create Docker Image

Run following command:
`docker build -t node-express-app .` <br>
*Note:*  You can change the name by replacing `node-express-app`

For further verification, you can check whether your image is successfully created by running following command:
`docker image ls`

### Build & Run Docker Container from Image

Simply run:
`docker container run -it --name node-express-app-container -p 9000:3000 node-express-app` <br>
*Note:* You can change the container name by replacing `node-express-app-container` & assign a free port to run the app instead of `3000` which is the local port for app listening by replacing `9000`

Once you successfully run the container, the Node - Express web application will be successfully running on http://localhost:9000 on your machine. 

### Create Docker Compose & Up the Whole Stack

Simply create a file as `docker-compose.yml` and add following content:

```
version: "3.9"
services:
  app:
	build: .
	image: "node-express-app:latest"
	ports:
	  - "9000:3000"
```
Now you can simply up the whole stack by runnnig `docker compose up` anytime. <br>

The app will be live on: https://localhost:9000 

### Push Docker Image to GitLab Container Registry

Run following commands:

Signin to GitLab: `docker login registry.gitlab.com`

Create an image under repository: `docker build -t registry.gitlab.com/devops_practices/node-express-dockerized-app .` 

Push image to Container Registry: `docker push registry.gitlab.com/devops_practices/node-express-dockerized-app`
